import bApi as bApi
import time
import csv

fee = .1

def buyAdvised(token, secretKey, symbol, threshhold, startingPrice, trail=False):
	#returns true if price moves down a certain percent
	if len(bApi.getOpenOrders(token, secretKey, symbol))>0:
		return False
	if trail:
		threshhold = threshhold + (threshhold*.1)
	currentPrice = float(bApi.getPrice(token,symbol)['price'])
	currentDifference = (1-(currentPrice/float(startingPrice)))*100
	print("CURRENTDIFFERENCE: {}".format(currentDifference))
	if currentDifference>=threshhold:
		print("buy advised\nstartingPrice: {0}\ncurrentPrice: {1}".format(startingPrice, currentPrice))
		if trail:
			return float(startingPrice)-(float(startingPrice)*((float(currentDifference)/100)*.7))
		return float(currentPrice)
	print("not advised\nstartingPrice: {0}\ncurrentPrice: {1}".format(startingPrice, currentPrice))
	return False

def sellAdvised(token, secretKey, symbol, threshhold, startingPrice, trail=False):
	#returns true if price up a certain percent
	print("getting open orders")
	if len(bApi.getOpenOrders(token, secretKey, symbol))>0:
		return False
	if trail:
		threshhold = threshhold + (threshhold*.1)
	print("getting current price")
	currentPrice = float(bApi.getPrice(token,symbol)['price'])
	currentDifference = ((currentPrice-float(startingPrice))/float(startingPrice))*100
	print("CURRENTDIFFERENCE: {}".format(currentDifference))
	if currentDifference>=threshhold:
		print("sell advised\nstartingPrice: {0}\ncurrentPrice: {1}".format(startingPrice, currentPrice))
		if trail:
			return float(startingPrice)+(float(startingPrice)*((float(currentDifference)/100)*.7))
		return float(currentPrice)
	print("not advised\nstartingPrice: {0}\ncurrentPrice: {1}".format(startingPrice, currentPrice))
	return False


def pull_file_contents(filename):
	file_data = []
	with open(filename, 'rt') as csvfile:
	    spamreader = csv.DictReader(csvfile)
	    for row in spamreader:
	    	file_data.append(row)
	return file_data

def testGetBalances(symbol):
	logFile = pull_file_contents("log.csv")
	lastEntry = len(logFile)-1
	b1 = logFile[lastEntry]['Balance{}'.format(symbol[:3])]
	b2 = logFile[lastEntry]['Balance{}'.format(symbol[3:])]
	return{"b1":b1,"b2":b2}


def record(method, price, gain, balance1, balance2, headers=None):
	localTime = time.asctime(time.localtime(time.time()))
	message = "{0},{1},{2},{3},{4},{5}".format(localTime, method, price, gain, balance1, balance2)
	with open('log.csv', 'a') as logfile:
		if headers:
			logfile.write(headers+'\n')
		logfile.write(message + '\n')


def testTrailingStopLoss(token, secretKey, symbol, threshhold, method, quantity, startingPrice=None, fee=fee):
	#returns false if no trade is made
	print("trailing stop loss")
	if startingPrice==None:
		print("setting starting price")
		startingPrice=float(bApi.getPrice(token, symbol)['price'])
	initialPrice = startingPrice
	print("grabbing price")
	price = sellAdvised(token, secretKey, symbol, threshhold, startingPrice, trail=True)
	balances = testGetBalances(symbol)
	b1 = balances['b1']
	b2 = balances['b2']
	if price:
		diff = 1-(float(price)/float(initialPrice))*100
		print("sale advised")
		startingPrice = price
		while float(bApi.getPrice(token,symbol)['price'])>=float(startingPrice):
			time.sleep(3)
			price = sellAdvised(token, secretKey, symbol, threshhold, startingPrice, trail=True)
			diff = 1-(float(price)/float(initialPrice))*100
			b1 = (b2*(1-(float(fee)/100)))*price
			b2 = 0
			if price:
				print("moving order up")
				startingPrice=price
				record("INNER "+method, startingPrice, diff, b1, b2)
		record(method, startingPrice, diff, b1, b2)
		return startingPrice
	return False


def testTakeProfitLimit(token, secretKey, symbol, threshhold, method, quantity=None, startingPrice=None, fee=fee):
	#returns false if no trade is made
	print("take profit limit")
	if startingPrice==None:
		print("setting starting price")
		startingPrice=float(bApi.getPrice(token, symbol)['price'])
	initialPrice = startingPrice
	print("grabbing price")
	price = buyAdvised(token, secretKey, symbol, threshhold, startingPrice, trail=True)
	balances = testGetBalances(symbol)
	b1 = balances['b1']
	b2 = balances['b2']
	if price:
		print("buy advised")
		diff = (float(initialPrice)/float(price))*100
		startingPrice = price
		while float(bApi.getPrice(token,symbol)['price'])<=float(startingPrice):
			time.sleep(3)
			price = buyAdvised(token, secretKey, symbol, threshhold, startingPrice, trail=True)
			diff = (float(initialPrice)/float(price))*100
			b2 = (b1*(1-(float(fee)/100)))*price
			b1 = 0
			if price:
				print("moving order down")
				startingPrice=price
				record("INNER "+method, startingPrice, diff, b1, b2)
		record(method, startingPrice, diff, b1, b2)
		return startingPrice
	return False

def initializeLog(token, symbol):
	print("initializing")
	price = bApi.getPrice(token, symbol)['price']
	record("NONE", price, 0, 1, 0, headers="Time,Method,Price,Gain,BalanceLTC,BalanceBTC")
	print("initialized")
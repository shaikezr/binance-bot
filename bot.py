import time
import bApi as bApi
from bTest import initializeLog, testTrailingStopLoss, testTakeProfitLimit
import sys

token = "TOKEN"
secretKey = b"SECRET KEY"

baseURL = "https://api.binance.com"
pingURL = "/api/v1/ping"
timeURL = "/api/v1/time"
bookURL = "/api/v1/depth"
recentTradeURL = "/api/v1/trades"
historicalTradeURL = "/api/v1/historicalTrades"
rangeTradeURL = "/api/v1/aggTrades"
candlestickURL = "/api/v1/klines"
dayURL="/api/v1/ticker/24hr"
priceURL = "/api/v3/ticker/price"
bookTickerURL="/api/v3/ticker/bookTicker"
testNewOrderURL = "/api/v3/order/test"
orderURL="/api/v3/order"
openOrdersURL="/api/v3/openOrders"
allOrdersURL="/api/v3/allOrders"
accountInfoURL="/api/v3/account"
accountTradesURL="/api/v3/myTrades"



def buyAdvised(token, secretKey, symbol, threshhold, startingPrice, trail=False):
	#returns true if price moves down a certain percent
	if len(bApi.getOpenOrders(token, secretKey, symbol))>0:
		return False
	if trail:
		threshhold = threshhold + (threshhold*.1)
	currentPrice = float(bApi.getPrice(token,symbol)['price'])
	currentDifference = (1-(currentPrice/float(startingPrice)))*100
	if currentDifference>=threshhold:
		print("buy advised\nstartingPrice: {0}\ncurrentPrice: {1}".format(startingPrice, currentPrice))
		if trail:
			return float(startingPrice)-(float(startingPrice)*((float(currentDifference)/100)*.7))
		return float(currentPrice)
	print("not advised\nstartingPrice: {0}\ncurrentPrice: {1}".format(startingPrice, currentPrice))
	return False

def sellAdvised(token, secretKey, symbol, threshhold, startingPrice, trail=False):
	#returns true if price up a certain percent
	print("getting open orders")
	if len(bApi.getOpenOrders(token, secretKey, symbol))>0:
		return False
	if trail:
		threshhold = threshhold + (threshhold*.1)
	print("getting current price")
	currentPrice = float(bApi.getPrice(token,symbol)['price'])
	currentDifference = ((currentPrice-float(startingPrice))/float(startingPrice))*100
	if currentDifference>=threshhold:
		print("sell advised\nstartingPrice: {0}\ncurrentPrice: {1}".format(startingPrice, currentPrice))
		if trail:
			return float(startingPrice)+(float(startingPrice)*((float(currentDifference)/100)*.7))
		return float(currentPrice)
	print("not advised\nstartingPrice: {0}\ncurrentPrice: {1}".format(startingPrice, currentPrice))
	return False


def trailingStopLoss(token, secretKey, symbol, threshhold, method, quantity, startingPrice=None):
	#returns false if no trade is made, otherwise returns the price the trade was made at
	print("trailing stop loss")
	if startingPrice==None:
		print("setting starting price")
		startingPrice=bApi.getPrice(token, symbol)['price']
	print("grabbing price")
	price = sellAdvised(token, secretKey, symbol, threshhold, startingPrice, trail=True)
	if price:
		print("sale advised")
		orderId = bApi.newOrder(token, secretKey, symbol, "SELL", "STOP_LOSS_LIMIT", quantity, timeInForce="GTC", price=price)['orderId']
		startingPrice = price
		while len(bApi.getOpenOrders(token, secretKey, symbol))>0:
			time.sleep(3)
			price = sellAdvised(token, secretKey, symbol, threshhold, startingPrice, trail=True)
			if price:
				print("moving order up")
				bApi.cancelOrder(token, secretKey, symbol, orderId=orderId)
				orderId = bApi.newOrder(token, secretKey, symbol, method, "STOP_LOSS_LIMIT", quantity, timeInForce="GTC", price=price)['orderId']
				startingPrice=price
		return startingPrice
	
	return False


def takeProfitLimit(token, secretKey, symbol, threshhold, method, quantity=None, startingPrice=None):
	#returns false if no trade is made otherwise returns the price the trade was made at
	print("take profit limit")
	if startingPrice==None:
		print("setting starting price")
		startingPrice=bApi.getPrice(token, symbol)['price']
	print("grabbing price")
	price = buyAdvised(token, secretKey, symbol, threshhold, startingPrice, trail=True)
	if price:
		print("buy advised")
		orderId = bApi.newOrder(token, secretKey, symbol, "BUY", "TAKE_PROFIT_LIMIT", (quantity/price), timeInForce="GTC", price=price)['orderId']
		startingPrice = price
		while len(bApi.getOpenOrders(token, secretKey, symbol))>0:
			time.sleep(3)
			price = buyAdvised(token, secretKey, symbol, threshhold, startingPrice, trail=True)
			if price:
				print("moving order down")
				bApi.cancelOrder(token, secretKey, symbol, orderId=orderId)
				orderId = bApi.newOrder(token, secretKey, symbol, method, "TAKE_PROFIT_LIMIT", (quantity/price), timeInForce="GTC", price=price)['orderId']
				startingPrice=price
		return startingPrice
	return False

def tradeBotUsingStops(token, secretKey, symbols, threshhold, method="SELL", startingPrice=None, test=False):
	###USE THIS###
	currentBalance = {}
	symbol = ""
	print("setting balances")
	balances = bApi.getAccountInfo(token, secretKey)['balances']
	for i in symbols:
		currentBalance[i]=[(float(x['free'])+float(x['locked'])) for x in balances if x['asset']==i][0]
		symbol+=i
	startingBalance = currentBalance
	print("balances set: {}".format(startingBalance))
	gain = 0
	if startingPrice == None:
		startingPrice = bApi.getPrice(token, symbol)['price']
	sell = False
	buy = False
	if method=="FIRST":
		method = initialAdvisor(token, secretKey, symbol, threshhold, startingPrice, trail=True)
	while True:
		if method == "SELL":
			if buy:
				startingPrice = buy
			if test:
				sell = testTrailingStopLoss(token, secretKey, symbol, threshhold, method, currentBalance['{}'.format(symbols[0])], startingPrice)
			else:
				sell = trailingStopLoss(token, secretKey, symbol, threshhold, method, currentBalance['{}'.format(symbols[0])], startingPrice)
			if sell:
				print("sold: {}".format(sell))
				method = "BUY"
				time.sleep(3)
		if method == "BUY":
			if sell:
				startingPrice = sell
			if test:
				buy = testTakeProfitLimit(token, secretKey, symbol, threshhold, method, currentBalance['{}'.format(symbols[1])], startingPrice)
			else:	
				buy = takeProfitLimit(token, secretKey, symbol, threshhold, method, currentBalance['{}'.format(symbols[1])], startingPrice)
			if buy:
				print("bought: {}".format(buy))
				method = "SELL"
				time.sleep(3)

def initialAdvisor(token, secretKey, symbol, threshhold, startingPrice, trail=False):
	buy = False
	sell = False
	while buy==False and sell==False:
		buy = buyAdvised(token, secretKey, symbol, float(threshhold*.5), startingPrice, trail)
		sell = sellAdvised(token, secretKey, symbol, float(threshhold*.5), startingPrice, trail)
		if type(buy) != type(True):
			float(buy)
			print("Trending towards buy: {}".format(buy))
			return "BUY"
		else:
			print("Buy Advised: {}".format(buy))
		if type(sell) != type(True):
			float(sell)
			print("Trending towards sell: {}".format(sell))
			return "SELL"
		else:
			print("Sell Advised: {}".format(sell))



def getBalances(symbols, token, secretKey):
	currentBalances ={}
	for i in symbols:	
		currentBalances[i]=[(float(x['free'])+float(x['locked'])) for x in getAccountInfo(token, secretKey)['balances'] if x['asset']==i][0]
	return currentBalances



def main(symbolPair, symbolArray, test):
	initializeLog(token, symbolPair)
	tradeBotUsingStops(token, secretKey, symbolArray, .8, method="FIRST", test=test)

main("LTCBTC", ["LTC","BTC"], test=True)

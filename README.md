DISCLAIMER:

I assume no responsibility for your use of this API. Moreover I have never tested this with live trades. Because of this, I can't verify that the bot runs without error when using live trades. Use at your own risk.


Quick start:

1. Open bot.py and enter your binance secret key & token (see https://support.binance.com/hc/en-us/articles/360002502072-How-to-create-API)
2. Set your currency pair in the main() function at the bottom of bot.py (keep test=True unless you want to send trades)
3. run boy.py

Architecture:

-bApi.py contains the functions needed to make API calls to binance
-bot.py contains the trading algorithm
-bTest.py contains the functions needed to simulate trading
-When test=True, bTest is called from tradeBotUsingStops in bot.py
    
Trading Algorithm:


overview:
The intention is to follow price movements instead of trading on margins. Instead of setting a buy at 1% then a sell at 1%, the bot will watch for price movements, then set stop loss/take profits at prices above/below (respectively) market rates. It will continue doing this until the order is executed. I am trying to replicate a trailing stop loss/trailing take profit (see https://www.interactivebrokers.com/en/index.php?f=606 and https://www.investopedia.com/articles/trading/08/trailing-stop-loss.asp)

steps:
If method = first inside of the main() function (param of function tradeBotUsingStops), then the bot will assume you have both currencies and will select the option (buy/sell) that seems profitable, determined by a threshhold [param in tradeBotUsingStops] percent movement from the starting price. Default is .8%. 

If method = "SELL", the bot looks for price movements up. If method = "BUY", the bot looks for price movements down.

ex. method = "FIRST". If  XRP is $.35, and the price moves to $.40, then the bot will advise selling the XRP. It does this by setting a stop loss limit below the current price. If the price continues going up, the bot will cancel the previous stop loss limit and make a new stop loss limit at a higher price. It will do this until there are no open orders (trade has been executed), then it will switch to a buy. At this point, the bot will watch for the price to drop. if it drops below the threshhold percentage movement, then the bot will advise a buy and a take profit limit will be placed above the current price. If the price continues going up, the bot will cancel the previous take profit limit and make a new take profit limit at a lower price.


